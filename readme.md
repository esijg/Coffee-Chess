# Coffee Chess

A strategy board game for 2 players about coffee, made out of coffee. An odd mixture of Chess, Tic tac toe and Risk.

Requirements:
- 8x8 Chess Board
- 50 Coffee Beans