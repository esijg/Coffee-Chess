% Coffee Chess v1.0
% Jóhannes Gunnar Þorsteinsson
% New-Year Game Jam 2012-2013

![](/Users/JohannesG/Documents/Verkefni/Coffee Chess/Source/Images/Cover.jpg)

# What is Coffee Chess?
A strategy board game for 2 players about coffee, made out of coffee. An odd mixture of Chess, Tic tac toe and Risk.

# Required Items
8x8 Chess Board  
50 coffee beans or any other similar small items.  
Coffee beans are recommended as they make your hands smell in a wonderful way.  

# Rules

## Setting up
Players start by deciding which square colours they will play on. Like in chess, the white player makes the first turn. The player that plays on white squares can only place and move beans on white squares, and vice versa.

Players then set all the 50 beans next to the game area. This will be their shared bank. When the bank runs out of beans, the game ends.

## A Turn
At the beginning of each round, a player gets 2 beans put into his inventory. He can choose to not play them out and instead save them as long as he wants. The inventory can only hold 5 beans maximum though.

### Action types
Action types are the actions a player can do during his turn. Note that each action costs 1 bean from the inventory, and you can only do one action type per turn. You may do them as often as you want as long as you have beans in the inventory to spare. But you can't do two types of actions in one turn. You must choose between either placing out beans, moving beans or stealing beans. Saving beans in inventory does not count as an action.

#### Placing out beans
This is the standard action where the player places beans on his respective squares. Note that this action technically doesn't "cost" a bean as you just move a bean from your inventory to the game board.

#### Moving beans
If you have more than 1 bean on a square you can move them all to any connected diagonal square, but you must always leave one bean behind. Moving beans costs one bean from your inventory. That one bean is returned to the shared bank.

#### Stealing beans
You can steal enemy beans from any adjacent square to your square for the cost of one bean from your inventory which is returned to the shared bank. You can steal as many beans from the enemy square as you have in your square. But you can not steal to the same square more than once each turn. For example, if the enemy has 2 beans on his square and you have 3, then you are allowed to take 2 of his beans at the cost of 1 from your inventory. On the other hand, if you have 2 squares with 1 bean each and the enemy has 2 at a connected square you will have to spend 2 actions(beans) to be able to empty his square.

## Progression

### Three squares in a row
Putting beans in three squares in a diagonal row will give you +1 bean in the next turn and every turn after that unless that row is disturbed by the enemy player by the act of stealing. Squares can not be counted twice. For example, you can not fill in 5 squares in an "X" and count that as +2 beans per turn. In other words, intersecting lines do not count.

5 beans in the inventory per turn is the max.

## Winning 
When the shared bean bank is finished, the player with the most beans on the board wins.
	
## Reward
The loser brews coffee for the winner. Both loser and winner are rewarded with their hands smelling of delicious coffee beans.

# Example Turns
1. **Player 1** gains **2 beans**, puts them out next to each other on the game board.
2. **Player 2** gains **2 beans**, saves them.
3. **Player 1** gains **2 beans**, puts one next to his previous beans to form a **3 in a row**. Saves the last bean.
4. **Player 2** gains **2 beans** and now has **4 beans** in his inventory. He places out **3 in a diagonal line** close to the opponent. and puts his last one on one of his already occupied squares to make it harder to steal.
5. **Player 1** gains **3 beans** and now has **4 beans** in his inventory. he steals 1 bean from each enemy square at the cost of **3 beans** but leaves **1 bean** in his inventory.
6. **Player 2** gains **2 beans** and places them on his last remaining occupied square.
7. **Player 1** gains **3 beans** and uses them to retreat from 3 of his squares, leaving **1 bean** in his inventory.

# Credits
Jóhannes Gunnar Þorsteinsson - Game Design  
johannesg@johannesg.com

Göran Svensson - Beta testing and invaluable feedback

# Related Links
http://www.johannesg.com

http://nygj.blechi.at